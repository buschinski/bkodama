#!/bin/sh

if [ ! -d ../data ]; then
	mkdir ../data
fi

sh generatesvg.sh
sh createicons.sh
