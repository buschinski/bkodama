#!/bin/bash

ICONSVG=icon00.svg

for i in 16 32 64 128
do
	inkscape -z -f $ICONSVG -e ../data/hi$i-app-bkodama.png \
	--export-width=$i --export-height=$i
done

inkscape -z -f $ICONSVG -l ../data/hisc-app-bkodama.svgz --vacuum-defs
